const express = require('express');

// creating a server using express
const app = express();

// port
const port = 4000;

// middlewares
app.use(express.json());
	// allows app to read a json data
app.use(express.urlencoded({extended: true}));


// GET route

app.get('/home', (req, res) => {
	res.send('Welcome Home')
});

// GET route users

let users = [
	{
		email: "nezukoKamado@gmail.com",
		username: "nezuko01",
		password: "letMeOut",
		isAdmin: false
	},
	{
		email: "tanjiroKamado@gmail.com",
		username: "gonpanchiro",
		password: "iAmTanjiro",
		isAdmin: false
	},
	{
		email: "zenitsuAgatsuma@gmail.com",
		username: "zenitsuSleeps",
		password: "iNeedNezuko",
		isAdmin: true
	}

]

let loggedUser;


app.get('/users', (req, res) => {
	res.send('Please login')
});


app.delete('/users/delete-user', (req, res) => {

	// store the message that will be sent back to our client
	let message;

	// will loop through all the "users" array.
	for(let i = 0; i < users.length; i++){

		// if the username provided in the request is the same with the username in loop
		if(req.body.username === users[i].username){

			// change the password of the user found in the loop by the requested password in the body by the client
			users[i].password = req.body.password;

			// send a message to the client
			message = `User ${req.body.username}'s password has been changed.`

			// break the loop once  a user matches the username provided in the client
			break;

		// if no user was found
		}else{

			// change the message to be sent back as a response
			message = 'User not found.'
		}
	}

	// response that will be sent to our client.
	res.send(message)
})




// app.delete('/users', (req, res) => {
// 	res.send(`Delete request at ${user}`)
// });


app.listen(port, () => console.log(`The Server is running at port ${port}`));

